<!DOCTYPE html>
<html>
<head>
  <title>User Registration | PHP</title>
</head>
<body>
<div>
  <form action="registration.php" method="post">
    <div class="container">
      <h1>Registration</h1>
      <p>Fill up the form with correct values.</p>
      <label1 for="firstname"><b>First Name</b></label>
      <input type="text" name="firstname" required>
      <label1 for="Lastname"><b>Last Name</b></label>
      <input type="text" name="lastname" required>
      <label1 for="email"><b>Email-Address</b></label>
      <input type="email" name="email" required>
      <label1 for="phonenumber"><b>Phone Number</b></label>
      <input type="text" name="phonenumber" required>
      <label1 for="password"><b>Password</b></label>
      <input type="password" name="password" required>
 
     </div>
   </form>
 </div>
</body>
</html>